import {Context, createContext, FC, lazy, PropsWithChildren, Suspense, useContext, useState} from 'react'
import './App.css'
import reactLogo from './assets/react.svg'

interface ContextAValue {
    count: number;
}

interface ContextBValue extends ContextAValue {
    setCount(callback: (state: number) => number): void;
}

const ContextA = createContext<ContextAValue>({count: -1})
const ContextB = createSubContext<ContextBValue, ContextAValue>(ContextA, {
    count: -2,
    setCount() {
        throw Error("not implemented")
    }
})

function createSubContext<T extends TBase, TBase>(BaseContext: Context<TBase>, defaultValue: T): Context<T> {
    const NewContext = createContext<T>(defaultValue);
    const NewProvider = NewContext.Provider;
    // @ts-ignore
    NewContext.Provider = ({children, value}) => {
        return (
            <NewProvider value={value}>
                <BaseContext.Provider value={value}>
                    {children}
                </BaseContext.Provider>
            </NewProvider>
        )
    }
    return NewContext;
}

const ContextBProvider: FC<PropsWithChildren> = ({children}) => {
    const [count, setCount] = useState(0)

    return (
        <ContextB.Provider value={{count, setCount}}>
            {children}
        </ContextB.Provider>
    );
}

const MaybeLazyComponent: FC = () => {
    return (
        <span>lazy component</span>
    );
}

const LazyComponent = lazy(async () => ({default: MaybeLazyComponent}))

function App() {
    return (
        <ContextBProvider>
            <div className="App">
                <div>
                    <a href="https://vitejs.dev" target="_blank">
                        <img src="/vite.svg" className="logo" alt="Vite logo"/>
                    </a>
                    <a href="https://reactjs.org" target="_blank">
                        <img src={reactLogo} className="logo react" alt="React logo"/>
                    </a>
                </div>
                <h1>Vite + React</h1>
                <Suspense fallback={"upsi!"}>
                    <LazyComponent/>
                </Suspense>
                <div className="card">
                    <CounterButton/>
                    <CounterDisplay/>
                </div>
            </div>
        </ContextBProvider>
    )
}

function CounterButton() {
    const {count, setCount} = useContext(ContextB);
    return (
        <button onClick={() => setCount((count) => count + 1)}>
            count is {count}
        </button>
    );
}

function CounterDisplay() {
    const {count} = useContext(ContextA);

    return (
        <span>Current count is {count}</span>
    );
}

export default App
